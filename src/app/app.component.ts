import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private http:HttpClient, private modalService:NgbModal) {}

  stylesAvailable: boolean=false;
  images:any;
  fileChosen: boolean=false;
  filesChosen: boolean=false;
  flag:any;
  showProgress: boolean=false;
  selectedStyle: any;
  checkout: boolean=false;
  color:any;
  style:any;
  category:any;
  file:any;
  files:any;
  sourceImage:any;
  sourceImage1:any;
  sourceImage2:any;
  showRedesignAttribute:boolean=false;
  showRedesignProgress:boolean=false;
  showMixMatchAttribute:boolean=false;
  showMixMatchProgress:boolean=false;
  userSelected:boolean=false;
  mixUserSelected:boolean=false;
  userType: any;
  storeValidated:boolean=false;
  mixStoreValidated:boolean=false;
  storeName='BWR Clothing "Buy Wear Repeat"';
  storeAddress='Gandhi Bazaar, Basavangudi, Bangalore, Karnataka 560004';
  phone='07353433588';
  email='bwrclothing@gmail.com';
  gst='12345ABC';
  firstName: any;
  lastName: any;
  showRedesignModel:boolean=false;
  showMixMatchModel:boolean=false;
  showRedesignVirtualProgress:boolean=false;
  showVirtualTryOnProgress:boolean=false;
  showModelledImage:boolean=false;
  redesign:boolean=false;
  mixmatch:boolean=false;
  uploadedImageName='';
  redesignIndex: any;
  mixMatchIndex:any;
  closeResult:any;
  typeOfUser:any;

  fileSelected(event: { target: any; }) {
    this.fileChosen=true;
    if(event.target.files.length > 0) {
      this.uploadedImageName=event.target.files[0].name;
      this.sourceImage='assets/images/inputs/'+event.target.files[0].name;
      this.showRedesignProgress=true;
      setTimeout(() => {
        this.showRedesignAttribute=true;
        this.showRedesignProgress=false;
      },2000);
    }
  }

  open(content: any) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  openRefurbishmentModal(content: any) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  connectWithStore(content: any) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  confirmedStore() {
    alert('Store has been notified with your requirement!');
    this.modalService.dismissAll('closing all');
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  redesignImageClicked(i: any) {
    this.showRedesignModel=true;
    this.redesignIndex=i;
  }

  mixMatchImageClicked(i: any) {
    if(i==1) {
        this.showRedesignVirtualProgress=true;
        setTimeout(() => {
          this.showVirtualTryOnProgress=true;
          this.showRedesignVirtualProgress=false;
          this.showModelledImage=true;
        },2000);
    }
    this.showMixMatchModel=true;
    this.mixMatchIndex=i;
  }

  redesignModelUploaded(event: { target: any; }) {
    if(event.target.files.length > 0) {
      this.showRedesignVirtualProgress=true;
      setTimeout(() => {
        this.showVirtualTryOnProgress=true;
        this.showRedesignVirtualProgress=false;
        this.showModelledImage=true;
      },2000);
    }
  }

  mixMatchModelUploaded(event: { target: any; }) {
    if(event.target.files.length > 0) {
      this.showRedesignVirtualProgress=true;
      setTimeout(() => {
        this.showVirtualTryOnProgress=true;
        this.showRedesignVirtualProgress=false;
        this.showModelledImage=true;
      },2000);
    }
  }

  filesSelected(event: { target: any; }) {
    this.filesChosen=true;
    if(event.target.files.length > 0) {
      this.sourceImage1='assets/images/inputs/'+event.target.files[0].name;
      this.sourceImage2='assets/images/inputs/'+event.target.files[1].name;
      this.showMixMatchProgress=true;
      setTimeout(() => {
        this.showMixMatchAttribute=true;
        this.showMixMatchProgress=false;
      },1000);    
    }
  }

  scrollToGallery = function() {
    window.scrollTo(0, 2600);
  }

  getStarted = function() {
    window.scrollTo(0, 1450);
  }

  getMixMatch = function() {
    window.scrollTo(0,1700);
  }

  contactSection = function() {
    window.scrollTo(0,3000);
  }

  confirmUser(userType: any) {
     if(userType.indexOf('mix')>=0) {
       this.mixUserSelected=true;
       this.userType = userType;
     }
     else {
       this.userSelected = true;
       this.userType = userType;
     }     
  }

  validateStore() {
    this.storeValidated=true;
  }

  mixValidateStore() {
    this.mixStoreValidated=true;
  }

  getRefurbishmentStores = function() {

  }

  back() {
    console.log('back');
    this.showMixMatchModel=false;
    this.showModelledImage=false;
  }

  getAllStyles(flag: any) {
    this.scrollToGallery();
    this.showProgress=true;
    this.showRedesignModel=false;
    this.showMixMatchModel=false;
    this.showRedesignVirtualProgress=false;
    this.showVirtualTryOnProgress=false;
    this.showModelledImage=false;
    this.redesign=false;
    this.mixmatch=false;
    this.uploadedImageName='';
    setTimeout(() => {
      this.flag=flag;
      if(flag=='redesign') {
          this.stylesAvailable=true;
          this.redesign=true;
          this.images = [
            {'img_name': 'assets/images/blue_shirt_redesign.png', 'actualCost':'Rs.2000-Rs.2500', 'refurbishedCost':'Rs.300-Rs.500'},
            {'img_name': 'assets/images/denim2.jpg', 'actualCost':'Rs.1400-Rs.1500', 'refurbishedCost':'Rs.300-Rs.400'},
            {'img_name': 'assets/images/denim3.jpg', 'actualCost':'Rs.1600-Rs.2250', 'refurbishedCost':'Rs.300-Rs.460'}
          ]
          this.showProgress=false;
      }
      else {
          this.stylesAvailable=true;
          this.mixmatch=true;
          this.images = [
            {'img_name': 'assets/images/mixMatchOption1.png', 'actualCost':'Rs.2000-Rs.3000', 'refurbishedCost':'Rs.200-Rs.300'},
            {'img_name': 'assets/images/mixMatchOption2.png', 'actualCost':'Rs.2000-Rs.3500', 'refurbishedCost':'Rs.200-Rs.350'}
          ]
          this.showProgress=false;
      }      
    }, 2000);
  }
}
