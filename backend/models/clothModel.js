const mongoose = require('mongoose');

const clothes_data_set=new mongoose.Schema({
    _id: {type: String, required: false},
    cloth_style: {type: String, required: false},
    cloth_category: {type: String, required: true},
    colors: {type: String, required: false},
    img_name: {type: String, required: true}   
},{collection : 'clothes_data_set'});

module.exports = mongoose.model('clothes_data_set',clothes_data_set);  
