const express = require('express');
const router = express.Router();
const cloth = require('../models/clothModel');

router.get('/getAllStyles/:style/:category', async(req,res) => {
    try {
       const result = await cloth.find({cloth_style: req.params.style, cloth_category: req.params.category})
       res.json(result);
    } catch(err) {
       res.send('Error: '+err);
    }
})

module.exports = router;